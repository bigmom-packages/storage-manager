# Bigmom Storage Manager

## Warnings
1. Authentication is not included in this package. Add middleware to the config file to restrict access to the console.
2. The console included in this package should not be exposed to untrusted users. Ideally it should only be used by users trusted to not send modified requests, as size and MIME type of files are sent from the client side and not verified server side.
3. As of 2021-08-17, I couldn't get pre-signed requests with Content-Disposition to work for Digital Ocean Spaces.

## Introduction
This is a Laravel package for managing file storage, primarily for cases where more than one disk is used.

Primary motivation for this package is because there are cheaper storage services out there, though not as reliable. By uploading the same files to multiple services, a fallback solution can be implemented more easily than by uploading the same file to each service and having to make sure they are each up-to-date.

Package includes a console to manage the files, an endpoint and command to sync the three database tables between servers (production, staging, and console-exposed server, for example), and two Blade components with fallback implemented (x-bigmom-image and x-bigmom-source).

### Setup

1. `composer require bigmom/storage-manager`
2. `php artisan vendor:publish`
3. `php artisan migrate`

### Config

1. `expose-console` - Sets whether console should be exposed on this particular application.
2. `back-url` - Sets what the back button in the console should redirect to.
3. `disks.sequence` - Sequence for URLs to be returned for a particular file. Used to implement file fallback strategies.
4. `disks.config` - Configuration for each disk. Refer to the config file for more information.
5. `files.max-size` - Maximum file size that can be sent.
6. `allow-pull` - Sets whether an API endpoint to retrieve folders, files, and file versions should be exposed.
7. `api` - Details for the API endpoint to pull from.
8. `middleware` - Middleware stack for console and API endpoint.

### Commands

1. `php artisan storage:pull` - Pulls folders, files, and file versions from an API endpoint. This command uses Basic Auth, which might not be ideal for your particular situation. Consider carefully whether you want to use this authentication scheme.