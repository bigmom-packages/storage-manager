<?php

namespace Bigmom\StorageManager\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bigmom\StorageManager\Services\FileFinder
 */
class FileFinder extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'file-finder';
    }
}
