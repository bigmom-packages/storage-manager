<?php

namespace Bigmom\StorageManager\Commands;

use Bigmom\StorageManager\Models\BigmomFile;
use Bigmom\StorageManager\Models\BigmomFileVersion;
use Bigmom\StorageManager\Models\BigmomFolder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class PullStorageManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull data from remote storage manager.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::withBasicAuth(config('storage-manager.api.username'), config('storage-manager.api.password'))->get(config('storage-manager.api.url'));

        if (!$response->json()) {
            Log::error($response);
            return $this->info("Error " . $response->status());
        }

        $result = collect($response->json());

        DB::transaction(function () use ($result) {
            DB::delete('delete from bigmom_file_versions');
            DB::delete('delete from bigmom_files');
            DB::delete('delete from bigmom_folders');
            foreach ($result->get('folders') as $folder) {
                BigmomFolder::create($folder);
            }
            foreach ($result->get('files') as $file) {
                BigmomFile::create($file);
            }
            foreach ($result->get('file_versions') as $fileVersion) {
                BigmomFileVersion::create($fileVersion);
            }
        });
        
        $this->info('Success.');

        return 0;
    }
}
