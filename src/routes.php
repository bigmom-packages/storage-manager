<?php

use Bigmom\StorageManager\Http\Controllers\APIController;
use Bigmom\StorageManager\Http\Controllers\DiskController;
use Bigmom\StorageManager\Http\Controllers\FileController;
use Bigmom\StorageManager\Http\Controllers\FolderController;
use Bigmom\StorageManager\Http\Controllers\SignedStorageUrlController;
use Illuminate\Support\Facades\Route;

Route::prefix('/bigmom/storage-manager')->name('bigmom.storage-manager')->middleware(['web'])->group(function () {
    if (config('storage-manager.expose-console')) {
        Route::middleware(config('storage-manager.middleware.console'))->group(function () {
            Route::prefix('/api')->name('api.')->group(function () {
                Route::prefix('/folder')->name('folder.')->group(function () {
                    Route::get('/', [FolderController::class, 'index'])->name('index');
                    Route::post('/create', [FolderController::class, 'postCreate'])->name('postCreate');
                    Route::match(['delete', 'post'], '/delete', [FolderController::class, 'delete'])->name('delete');
                });
                Route::prefix('/file')->name('file.')->group(function () {
                    Route::get('/', [FileController::class, 'index'])->name('index');
                    Route::match(['put', 'post'], '/put', [FileController::class, 'putFile'])->name('putFile');
                    Route::match(['put', 'post'], '/upload', [FileController::class, 'upload'])->name('upload');
                    Route::post('/presign-url', [SignedStorageUrlController::class, 'store'])->name('store');
                    Route::post('/copy', [SignedStorageUrlController::class, 'copyFromTmp'])->name('copyFromTmp');
                    Route::post('/active', [FileController::class, 'setActiveFileVersion'])->name('setActiveFileVersion');
                    Route::match(['delete', 'post'], '/delete', [FileController::class, 'delete'])->name('delete');
                });
                Route::get('/disks', [DiskController::class, 'disks'])->name('disks');
            });
            Route::view('/{path?}', 'bigmom-storage-manager::app')->name('view');
        });
        if (config('storage-manager.allow-pull')) {
            Route::get('/api/pull', [APIController::class, 'pull'])->name('api.pull')->middleware(config('storage-manager.middleware.api-pull'));
        }
    }
});
