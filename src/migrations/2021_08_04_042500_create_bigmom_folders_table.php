<?php

use Bigmom\StorageManager\Models\BigmomFolder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBigmomFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bigmom_folders', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->unsignedBigInteger('parent_id')->nullable()->index();
            $table->foreign('parent_id')->references('id')->on('bigmom_folders');
            $table->string('parent_fqn')->nullable()->index();
            $table->string('fqn')->index();
            $table->timestamps();
        });

        BigmomFolder::create([
            'name' => '/',
            'parent_id' => null,
            'parent_fqn' => null,
            'fqn' => '/',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bigmom_folders');
    }
}
