<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBigmomFileVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bigmom_file_versions', function (Blueprint $table) {
            $table->id();
            $table->string('version_id')->index();
            $table->foreignId('bigmom_file_id')->index();
            $table->string('parent_fqn')->index();
            $table->string('fqn')->index();
            $table->string('type');
            $table->string('extension');
            $table->unsignedBigInteger('size');
            $table->boolean('is_public_read');
            $table->boolean('is_downloadable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bigmom_object_versions');
    }
}
