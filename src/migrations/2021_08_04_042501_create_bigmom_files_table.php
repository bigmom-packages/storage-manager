<?php

use Bigmom\StorageManager\Models\BigmomObject;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBigmomFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bigmom_files', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->foreignId('bigmom_folder_id')->index();
            $table->string('parent_fqn')->index();
            $table->string('fqn')->unique()->index();
            $table->string('active_version_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bigmom_files');
    }
}
