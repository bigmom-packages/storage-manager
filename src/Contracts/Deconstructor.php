<?php

namespace Bigmom\StorageManager\Contracts;

interface Deconstructor
{
    public function deconstruct(string $input): array;
}
