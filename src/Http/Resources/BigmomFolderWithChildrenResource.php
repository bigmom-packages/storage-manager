<?php

namespace Bigmom\StorageManager\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BigmomFolderWithChildrenResource extends BigmomFolderResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);

        $result['childFolders'] = BigmomFolderResource::collection($this->children);
        $result['files'] = BigmomFileResource::collection($this->files);

        return $result;
    }
}
