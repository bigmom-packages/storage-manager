<?php

namespace Bigmom\StorageManager\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BigmomFileResourceWithChildren extends BigmomFileResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = parent::toArray($request);

        $result['versions'] = BigmomFileVersionResource::collection($this->versions()->orderBy('updated_at', 'desc')->get());

        return $result;
    }
}
