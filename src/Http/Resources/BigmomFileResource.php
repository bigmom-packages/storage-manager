<?php

namespace Bigmom\StorageManager\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BigmomFileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'parentFqn' => $this->parent_fqn,
            'fqn' => $this->fqn,
            'url' => $this->url,
            'urls' => $this->urls,
            'type' => $this->type,
            'size' => $this->size,
            'activeVersionId' => $this->active_version_id,
        ];
    }
}
