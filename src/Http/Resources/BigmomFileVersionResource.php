<?php

namespace Bigmom\StorageManager\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BigmomFileVersionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'versionId' => $this->version_id,
            'parentFqn' => $this->parent_fqn,
            'fqn' => $this->fqn,
            'urls' => $this->urls,
            'url' => $this->url,
            'type' => $this->type,
            'extension' => $this->extension,
            'size' => $this->size,
        ];
    }
}
