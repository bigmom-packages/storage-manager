<?php

namespace Bigmom\StorageManager\Http\Controllers;

use Bigmom\StorageManager\Actions\DeleteFiles;
use Bigmom\StorageManager\Contracts\Deconstructor;
use Bigmom\StorageManager\Http\Resources\BigmomFileResourceWithChildren;
use Bigmom\StorageManager\Models\BigmomFile;
use Bigmom\StorageManager\Models\BigmomFileVersion;
use Bigmom\StorageManager\Services\FolderManager;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class FileController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'file' => ['required', 'string', 'max:21000', 'exists:bigmom_files,fqn'],
        ]);

        $file = BigmomFile::where('fqn', $request->input('file'))->firstOrFail();

        return response()->json(new BigmomFileResourceWithChildren($file));
    }

    public function putFile(Request $request, Deconstructor $deconstructor)
    {
        $request->validate([
            'fqn' => ['required', 'string', 'max:21000'],
            'type' => ['required', 'string', 'max:191'],
            'size' => ['required', 'integer', 'max:'.config('storage-manager.files.max-size')],
            'visibility' => ['required', 'string', 'max:191'],
            'is_downloadable' => ['required', 'boolean'],
        ]);

        $fqn = $request->input('fqn');

        $file = BigmomFile::where('fqn', $fqn)->first();

        $extension = pathinfo($fqn, PATHINFO_EXTENSION);

        $version = DB::transaction(function () use ($request, $fqn, $file, $deconstructor, $extension) {
            if ($file === null) {
                $deconstructedFqn = $deconstructor->deconstruct($fqn);

                $folderFqn = $deconstructedFqn[count($deconstructedFqn) - 2];

                $folder = (new FolderManager)->getAndCreateIfNotExist($folderFqn, $deconstructor);

                $splitFqn = explode('/', $fqn);

                $name = $splitFqn[count($splitFqn) - 1];

                $file = BigmomFile::create([
                    'name' => $name,
                    'bigmom_folder_id' => $folder->id,
                    'parent_fqn' => $folderFqn,
                    'fqn' => $fqn,
                ]);
            }

            $versionId = Uuid::uuid4();

            $version = BigmomFileVersion::create([
                'version_id' => $versionId,
                'bigmom_file_id' => $file->id,
                'parent_fqn' => $fqn,
                'fqn' => $fqn . '/' . $versionId,
                'type' => $request->input('type'),
                'extension' => $extension,
                'size' => $request->input('size'),
                'is_public_read' => $request->input('visibility') === 'public-read',
                'is_downloadable' => $request->input('is_downloadable'),
            ]);

            $file->active_version_id = $version->version_id;
            $file->save();

            return $version;
        });

        return response()->json([
            'message' => 'File put successfully.',
            'fqn' => $fqn . '/' . $version->version_id . '.' . $extension,
        ]);
    }

    public function upload(Request $request, Deconstructor $deconstructor)
    {
        $request->validate([
            'destination' => ['required', 'string', 'max:21000'],
            'file' => ['required', 'file', 'max:'.config('storage-manager.files.max-size')],
            'disk' => ['required', 'string', Rule::in(config('storage-manager.disks.sequence'))],
            'visibility' => ['required', 'string', 'max:191'],
            'content_disposition' => ['nullable', 'string', 'max:21000'],
        ]);

        $disk = $request->input('disk');

        $fqn = $request->input('destination');
        $deconstructedFqn = $deconstructor->deconstruct($fqn);

        $folderPrefix = config("storage-manager.disks.config.{$disk}.folder", '');
        $folderFqn = $folderPrefix . $deconstructedFqn[count($deconstructedFqn) - 2];

        $splitFqn = explode('/', $fqn);
        $name = $splitFqn[count($splitFqn) - 1];

        Storage::disk($disk)->putFileAs($folderFqn, $request->file('file'), $name, [
            'visibility' => $request->input('visibility') === 'public-read' ? 'public' : 'private',
            'ContentDisposition' => $request->input('content_disposition') ?: null,
        ]);

        return response()->json(['message' => 'File successfully uploaded']);
    }

    public function setActiveFileVersion(Request $request)
    {
        $request->validate([
            'file' => ['required', 'string', 'exists:bigmom_files,fqn'],
            'versionId' => ['required', 'string', 'exists:bigmom_file_versions,version_id'],
        ]);

        $file = BigmomFile::where('fqn', $request->input('file'))->firstOrFail();
        $file->active_version_id = $request->input('versionId');
        $file->save();

        return response()->json(['message', 'Version successfully set as active.']);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'file' => ['required', 'string', 'exists:bigmom_files,fqn'],
        ]);

        $file = BigmomFile::where('fqn', $request->input('file'))->firstOrFail();

        DB::transaction(function () use ($file) {
            foreach ($file->versions as $version) {
                DeleteFiles::delete($version);
                $version->delete();
            }
            $file->delete();
        });

        return response()->json(['message' => 'File successfully deleted.']);
    }
}
