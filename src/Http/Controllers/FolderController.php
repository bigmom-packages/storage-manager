<?php

namespace Bigmom\StorageManager\Http\Controllers;

use Bigmom\StorageManager\Http\Resources\BigmomFolderWithChildrenResource;
use Bigmom\StorageManager\Models\BigmomFolder;
use Bigmom\StorageManager\Services\FolderManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class FolderController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'folder' => ['required', 'string', 'max:21000', 'exists:bigmom_folders,fqn'],
        ]);
        
        $parentFolderId = $request->input('folder');

        $parentFolder = BigmomFolder::where('fqn', $parentFolderId)->first();

        return response()->json(new BigmomFolderWithChildrenResource($parentFolder));
    }

    public function postCreate(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:191', 'regex:/^[\w\d\-_ ]+$/'],
            'parentFqn' => ['required', 'string', 'max:21000', 'exists:bigmom_folders,fqn'],
        ]);

        $parentFqn = $request->input('parentFqn');
        $name = $request->input('name');

        $fqn = $parentFqn . $name . '/';

        if (BigmomFolder::where('fqn', $fqn)->exists()) {
            throw ValidationException::withMessages(['name' => 'Name is used by another folder.']);
        }
        
        $parentFolder = BigmomFolder::where('fqn', $parentFqn)->firstOrFail();

        $folder = BigmomFolder::create([
            'name' => $name,
            'parent_id' => $parentFolder->id,
            'parent_fqn' => $parentFqn,
            'fqn' => $fqn,
        ]);

        return response()->json(['message' => 'Folder created successfully.']);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'folder' => ['required', 'string', 'exists:bigmom_folders,fqn'],
        ]);

        $folder = BigmomFolder::where('fqn', $request->input('folder'))->firstOrFail();

        DB::transaction(function () use ($folder) {
            (new FolderManager)->deleteFolder($folder);
        });

        return response()->json(['message' => 'Folder deleted successfully.']);
    }
}
