<?php

namespace Bigmom\StorageManager\Http\Controllers;

use Aws\S3\S3Client;
use Bigmom\StorageManager\Contracts\Deconstructor;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

/**
 * Modified version of Laravel Vapor's SignedStorageUrlController (https://github.com/laravel/vapor-core)
 */
class SignedStorageUrlController extends Controller
{
    /**
     * Create a new signed URL.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'disk' => ['required', 'string', Rule::in(config('storage-manager.disks.sequence'))],
        ]);

        $disk = strtoupper($request->input('disk'));

        if ($disk === 'S3') $disk = 'AWS';

        $client = $this->storageClient($disk);

        $uuid = (string) Str::uuid();

        $bucket = $_ENV["${disk}_BUCKET"];

        $signedRequest = $client->createPresignedRequest(
            $this->createCommand($request, $client, $bucket, $key = ('tmp/'.$uuid)),
            '+5 minutes'
        );

        $uri = $signedRequest->getUri();

        return response()->json([
            'uuid' => $uuid,
            'bucket' => $bucket,
            'key' => $key,
            'url' => 'https://'.$uri->getHost().$uri->getPath().'?'.$uri->getQuery(),
            'headers' => $this->headers($request, $signedRequest),
        ], 201);
    }

    /**
     * Create a command for the PUT operation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Aws\S3\S3Client  $client
     * @param  string  $bucket
     * @param  string  $key
     * @return \Aws\Command
     */
    protected function createCommand(Request $request, S3Client $client, $bucket, $key)
    {
        return $client->getCommand('putObject', array_filter([
            'Bucket' => $bucket,
            'Key' => $key,
            'ACL' => $request->input('visibility') ?: $this->defaultVisibility(),
            'ContentType' => $request->input('content_type') ?: 'application/octet-stream',
            'CacheControl' => $request->input('cache_control') ?: null,
            'Expires' => $request->input('expires') ?: null,
            'ContentDisposition' => $request->input('content_disposition') ?: null,
        ]));
    }

    /**
     * Get the headers that should be used when making the signed request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \GuzzleHttp\Psr7\Request
     * @return array
     */
    protected function headers(Request $request, $signedRequest)
    {
        return array_merge(
            $signedRequest->getHeaders(),
            [
                'Content-Type' => $request->input('content_type') ?: 'application/octet-stream',
            ]
        );
    }

    /**
     * Get the S3 storage client instance.
     *
     * @return \Aws\S3\S3Client
     */
    protected function storageClient(string $disk)
    {
        $config = [
            'region' => config('filesystems.disks.s3.region', $_ENV["${disk}_DEFAULT_REGION"]),
            'version' => 'latest',
            'signature_version' => 'v4',
        ];

        $config['credentials'] = array_filter([
            'key' => $_ENV["${disk}_ACCESS_KEY_ID"] ?? null,
            'secret' => $_ENV["${disk}_SECRET_ACCESS_KEY"] ?? null,
            'token' => $_ENV["${disk}_SESSION_TOKEN"] ?? null,
        ]);

        if (array_key_exists("${disk}_URL", $_ENV) && ! is_null($_ENV["${disk}_URL"])) {
            $config['url'] = $_ENV["${disk}_URL"];
            $config['endpoint'] = $_ENV["${disk}_URL"];
        } else if (array_key_exists("${disk}_ENDPOINT", $_ENV) && ! is_null($_ENV["${disk}_ENDPOINT"])) {
            $config['url'] = $_ENV["${disk}_ENDPOINT"];
            $config['endpoint'] = $_ENV["${disk}_ENDPOINT"];
        }

        return new S3Client($config);
    }

    /**
     * Get the default visibility for uploads.
     *
     * @return string
     */
    protected function defaultVisibility()
    {
        return 'private';
    }

    public function copyFromTmp(Request $request, Deconstructor $deconstructor)
    {
        $request->validate([
            'destination' => ['required', 'string'],
            'disk' => ['required', 'string', Rule::in(config('storage-manager.disks.sequence'))],
            'key' => ['required', 'string', 'max:21000'],
        ]);

        $disk = $request->input('disk');

        $folderPrefix = config("storage-manager.disks.config.{$disk}.folder", '');
        $path = $folderPrefix . $request->input('destination');

        Storage::disk($disk)->copy($request->input('key'), $path);
    }
}
