<?php

namespace Bigmom\StorageManager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class DiskController extends Controller
{
    public function disks () {
        $disks = config('storage-manager.disks.sequence');

        $diskConfig = [];

        foreach ($disks as $disk) {
            $diskConfig[$disk] = config("storage-manager.disks.config.{$disk}");
        }

        return response()->json($diskConfig);
    }
}
