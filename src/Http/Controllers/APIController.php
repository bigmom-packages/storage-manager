<?php

namespace Bigmom\StorageManager\Http\Controllers;

use Bigmom\StorageManager\Models\BigmomFile;
use Bigmom\StorageManager\Models\BigmomFileVersion;
use Bigmom\StorageManager\Models\BigmomFolder;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function pull()
    {
        return response()->json([
            'folders' => BigmomFolder::get(),
            'files' => BigmomFile::get(),
            'file_versions' => BigmomFileVersion::get(),
        ]);
    }
}
