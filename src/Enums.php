<?php

namespace Bigmom\StorageManager;

class Enums
{
    const DISK_UPLOAD_TYPE = [
        'DIRECT' => 1,
        'PRESIGNED' => 2,
    ];
}
