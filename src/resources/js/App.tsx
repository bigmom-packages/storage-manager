import React, { ReactElement } from 'react'
import { Switch, Route } from 'react-router-dom'
import Navbar from './components/molecules/Navbar'
import Folder from './pages/Folder'
import { route } from './lib/helpers'
import Upload from './pages/Upload'
import File from './pages/File'

function App (): ReactElement {
  return (
    <div className="h-full w-full flex flex-col">
      <Navbar />
      <div className="flex-1">
        <Switch>
          <Route exact path={route('')}>
            <Folder />
          </Route>
          <Route exact path={route('/upload')}>
            <Upload />
          </Route>
          <Route exact path={route('/file')}>
            <File />
          </Route>
        </Switch>
      </div>
    </div>
  )
}

export default App
