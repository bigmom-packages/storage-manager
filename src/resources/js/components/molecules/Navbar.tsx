import React, { ReactElement } from 'react'
import { Link } from 'react-router-dom'
import { route } from '../../lib/helpers'
import { TertiaryButton } from '../atoms/Button'

export default function Navbar (): ReactElement {
  return (
    <div className="w-full bg-white">
      <div className="container mx-auto flex justify-between items-center">
        <div className="h-full flex items-end">
          <Link to={route('/')}><h4 className="text-xl font-bold px-4 py-4">Storage Manager</h4></Link>
        </div>
        <div className="h-full">
          <a href="/bigmom">
            <TertiaryButton>Back to Bigmom Home</TertiaryButton>
          </a>
        </div>
      </div>
    </div>
  )
}
