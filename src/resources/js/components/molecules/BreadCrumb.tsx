import React, { ReactElement, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { route } from '../../lib/helpers'
import { TertiaryButton } from '../atoms/Button'
import Card from '../atoms/Card'

interface Crumb {
  name: string
  fqn: string
}

export default function BreadCrumb ({ fqn, lastCrumbIsLink = false }: { fqn: string, lastCrumbIsLink?: boolean }): ReactElement {
  const [breadCrumb, setBreadCrumb] = useState<Crumb[]>([])
  useEffect(() => {
    const fqnArr = fqn.split('/')
    const newBreadCrumb: Crumb[] = [{
      name: 'Root',
      fqn: '/'
    }]
    fqnArr.forEach((name, index) => {
      if (name !== '') {
        const fqn = newBreadCrumb[index - 1].fqn + name + '/'
        newBreadCrumb.push({ name, fqn })
      }
    })
    setBreadCrumb(newBreadCrumb)
  }, [fqn])

  return (
    <Card className="w-full mb-8">
      {breadCrumb.map((crumb, index) => (
        <React.Fragment key={`crumb-${index}`}>
          {index > 0 ? <>&gt;</> : <></>}
          {lastCrumbIsLink || index !== breadCrumb.length - 1
            ? <Link to={route(`?folder=${crumb.fqn}`)}>
                <TertiaryButton className="mx-2">{crumb.name}</TertiaryButton>
              </Link>
            : <span className="mx-2 text-gray-600">{crumb.name}</span>}
        </React.Fragment>
      ))}
    </Card>
  )
}
