import React, { ReactElement } from 'react'

export default function Loading (): ReactElement {
  return (
    <div className="w-full h-full flex justify-center items-center">Loading...</div>
  )
}
