import React, { ReactElement } from 'react'

interface ErrorProps {
  message: string
}

export default function Error (props: ErrorProps): ReactElement {
  return (
    <div className="w-full h-full flex justify-center items-center text-lg font-bold">{ props.message }</div>
  )
}
