import axios from 'axios'
import { ReactElement, useEffect, useState } from 'react'
import { presignAndSend } from '../../lib/fileSenders'
import { route } from '../../lib/helpers'
import { useDisks } from '../../lib/SWR/hooks'
import { FileWithDirectory } from '../../typings/decs'
import Error from './Error'
import Loading from './Loading'

interface UploadItemProps {
  disk: string
  destination: string
  file: FileWithDirectory
  shouldUpload: boolean
  handleUploadDone: () => void
  visibilityIsPublic: boolean
  isDownloadable: boolean
}

type UploadStatus = 'Pending' | 'Uploading' | 'Success' | 'Failed'

export default function UploadItem (props: UploadItemProps): ReactElement {
  const { disk, destination, file, shouldUpload, handleUploadDone, visibilityIsPublic, isDownloadable } = props
  const [uploadStatus, setUploadStatus] = useState<UploadStatus>('Pending')
  const { disks, isLoading, error } = useDisks()
  useEffect(() => {
    if (shouldUpload) {
      startUpload()
    }
  }, [shouldUpload])

  async function startUpload () {
    setUploadStatus('Uploading')
    const visibility = visibilityIsPublic ? 'public-read' : 'private'
    if (disks[disk].is_presigned) {
      try {
        const options = {
          visibility: visibility
        } as Record<string, any>
        if (isDownloadable) {
          options.data = {
            'content_disposition': `attachment; filename=${file.name}`
          }
        }
        const data = await presignAndSend(file, disk, disks[disk].presign_url, options)
        await axios.post(route('api/file/copy'), {
          disk: disk,
          destination: destination,
          ...data
        })
        setUploadStatus('Success')
      } catch (err) {
        setUploadStatus('Failed')
        console.error(err)
      }
    } else {
      try {
        const formData = new FormData()
        formData.append('disk', disk)
        formData.append('destination', destination)
        formData.append('file', file)
        formData.append('visibility', visibility)
        if (isDownloadable) formData.append('content_disposition', `attachment; filename=${file.name}`)
        await axios.post(route('api/file/upload'), formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
        setUploadStatus('Success')
      } catch (err) {
        setUploadStatus('Failed')
        console.error(err)
      }
    }
    handleUploadDone()
  }

  if (isLoading) return <Loading />
  if (typeof error !== 'undefined') return <Error message={error.message} />

  return (
    <>
      <span>{uploadStatus}</span>
    </>
  )
}
