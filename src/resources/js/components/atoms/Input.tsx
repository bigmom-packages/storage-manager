import React, { InputHTMLAttributes, ReactElement } from 'react'
import cn from 'classnames'

export default function Input (props: InputHTMLAttributes<HTMLInputElement>): ReactElement {
  return (
    <input {...props} className={cn(props.className, 'px-4 py-2 bg-gray-200 focus:bg-gray-100 active:border')} />
  )
}
