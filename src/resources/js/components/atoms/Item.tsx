import React, { CSSProperties, ReactElement } from 'react'
import { FolderIcon, DocumentTextIcon } from '@heroicons/react/outline'

interface ItemInfoProps {
  text: string
  type: 'folder' | 'file'
}

export default function Item ({ text, type }: ItemInfoProps): ReactElement {
  return (
    <div className="bg-white shadow hover:shadow-lg py-4 px-2 flex items-center justify-center">
      {type === 'folder'
        ? <FolderIcon className="w-6 h-6 mr-2" />
        : <DocumentTextIcon className="w-6 h-6 mr-2" />}
      <p className="truncate w-4/5">{text}</p>
    </div>
  )
}
