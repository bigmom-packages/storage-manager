import React, { ReactElement } from 'react'
import { FolderIcon } from '@heroicons/react/outline'

interface FolderItemInfoProps {
  text: string
}

export default function FolderItem ({ text }: FolderItemInfoProps): ReactElement {
  return (
    <div className="bg-white shadow hover:shadow-lg py-4 flex items-center justify-center">
      <FolderIcon className="w-6 h-6 mr-2 hover:shadow-lg" />
      <p className="truncate w-4/5">{text}</p>
    </div>
  )
}
