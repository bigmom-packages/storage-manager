import { ReactElement, TdHTMLAttributes } from 'react'
import cn from 'classnames'

export function TD (props: TdHTMLAttributes<HTMLTableCellElement>): ReactElement<HTMLTableCellElement> {
  return (
    <td {...props} className={cn(props.className, 'px-2 py-2 border-b')} />
  )
}