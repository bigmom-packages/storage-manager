import React, { ReactElement, useRef, useState } from 'react'
import { DocumentDuplicateIcon, CheckCircleIcon, StarIcon as StarIconOutline } from '@heroicons/react/outline'
import { StarIcon as StarIconSolid } from '@heroicons/react/solid'
import { BigmomFileVersion } from '../../typings/models'
import { CopyToClipboard } from 'react-copy-to-clipboard'

interface FileVersionItemInfoProps {
  text: string
  version: BigmomFileVersion
  isActive: boolean
  setAsActive: (version: BigmomFileVersion) => void
}

export default function FileVersionItem ({ text, version, isActive, setAsActive }: FileVersionItemInfoProps): ReactElement {
  const [copyIsClicked, setCopyIsClicked] = useState(false)
  const copyTimeout = useRef<NodeJS.Timeout | undefined>()

  function handleCopy () {
    setCopyIsClicked(true)
    if (typeof copyTimeout.current !== 'undefined') {
      clearTimeout(copyTimeout.current)
    }
    copyTimeout.current = setTimeout(() => {
      setCopyIsClicked(false)
    }, 2000)
  }

  return (
    <div className="shadow px-4 hover:shadow-lg flex items-center justify-center bg-white">
      {isActive
        ? <StarIconSolid className="mr-2 w-6 h-6 text-yellow-600" />
        : <button onClick={() => { setAsActive(version) }}>
          <StarIconOutline className="mr-2 w-6 h-6 text-gray-600 hover:text-yellow-600" />
        </button>}
      <CopyToClipboard
        text={version.url}
        onCopy={handleCopy}
        >
        <div className="mr-2">
          {copyIsClicked
            ? <CheckCircleIcon className="w-6 h-6 text-green-500" />
            : <DocumentDuplicateIcon className="w-6 h-6 hover:text-blue-400 text-blue-600" />}
        </div>
      </CopyToClipboard>
      <a href={version.url} className="w-3/5 py-4 truncate">{text}</a>
    </div>
  )
}
