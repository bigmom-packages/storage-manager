import React, { HTMLAttributes, ReactElement } from 'react'
import cn from 'classnames'

export function ErrorMessage (props: HTMLAttributes<HTMLParagraphElement>): ReactElement {
  return <p {...props} className={cn(props.className, 'text-red-600')} />
}
