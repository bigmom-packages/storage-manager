import React, { ButtonHTMLAttributes, ComponentType, ReactElement } from 'react'
import cn from 'classnames'

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  Component?: ComponentType<any> | string
}

export function Button (props: ButtonProps): ReactElement {
  const { Component = "button", ...otherProps } = props
  return <Component {...otherProps} className={cn(props.className, 'px-4 py-2')} />
}

export function PrimaryButton (props: ButtonProps): ReactElement {
  return <Button {...props} className={cn('bg-primary border border-primary text-white font-bold hover:bg-primary-light hover:border-primary-light', props.className)} />
}

export function SecondaryButton (props: ButtonProps): ReactElement {
  return <Button {...props} className={cn('border border-primary text-primary hover:text-primary-light hover:border-primary-light', props.className)} />
}

export function TertiaryButton (props: ButtonProps): ReactElement {
  return <button {...props} className={cn('text-primary hover:text-primary-light', props.className)} />
}

export function DangerButton (props: ButtonProps): ReactElement {
  return <Button {...props} className={cn('border border-red-600 text-red-600 hover:text-red-400 hover:border-red-400', props.className)} />
}
