import { ReactElement, ThHTMLAttributes } from 'react'
import cn from 'classnames'

export function TH (props: ThHTMLAttributes<HTMLTableHeaderCellElement>): ReactElement<HTMLTableHeaderCellElement> {
  return (
    <th {...props} className={cn(props.className, 'px-2 py-2 border-b')} />
  )
}
