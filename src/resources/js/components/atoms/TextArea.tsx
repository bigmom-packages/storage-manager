import React, { ReactElement, TextareaHTMLAttributes } from 'react'
import cn from 'classnames'

export default function TextArea (props: TextareaHTMLAttributes<HTMLTextAreaElement>): ReactElement {
  return (
    <textarea {...props} className={cn(props.className, 'px-4 py-2 bg-gray-200 focus:bg-gray-100 active:border')} />
  )
}
