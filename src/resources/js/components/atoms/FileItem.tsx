import React, { ReactElement, useRef, useState } from 'react'
import { ExternalLinkIcon, DocumentDuplicateIcon, CheckCircleIcon } from '@heroicons/react/outline'
import { BigmomFile } from '../../typings/models'
import { Link } from 'react-router-dom'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { route } from '../../lib/helpers'

interface FileItemInfoProps {
  text: string
  file: BigmomFile
}

export default function FileItem ({ text, file }: FileItemInfoProps): ReactElement {
  const [copyIsClicked, setCopyIsClicked] = useState(false)
  const copyTimeout = useRef<NodeJS.Timeout | undefined>()

  function handleCopy () {
    setCopyIsClicked(true)
    if (typeof copyTimeout.current !== 'undefined') {
      clearTimeout(copyTimeout.current)
    }
    copyTimeout.current = setTimeout(() => {
      setCopyIsClicked(false)
    }, 2000)
  }

  return (
    <div className="bg-white shadow px-4 hover:shadow-lg flex items-center justify-center">
      <a href={file.url} className="w-6 h-6">
        <div className="hover:text-blue-400 text-blue-600 mr-2 w-full h-full">
          <ExternalLinkIcon className="w-full h-full" />
        </div>
      </a>
      <CopyToClipboard
        text={file.url}
        onCopy={handleCopy}
        >
        <div className="w-6 h-6 mr-2">
          {copyIsClicked
            ? <CheckCircleIcon className="w-full h-full text-green-500" />
            : <DocumentDuplicateIcon className="w-full h-full hover:text-blue-400 text-blue-600" />}
        </div>
      </CopyToClipboard>
      <Link to={route(`file?file=${file.fqn}`)} className="w-3/4 py-4 truncate">{text}</Link>
    </div>
  )
}
