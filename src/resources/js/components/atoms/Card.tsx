import React, { HTMLAttributes, ReactElement } from 'react'
import cn from 'classnames'

export default function Card (props: HTMLAttributes<HTMLDivElement>): ReactElement {
  return (
    <div {...props} className={cn(props.className, 'py-4 px-4 bg-white shadow')} />
  )
}
