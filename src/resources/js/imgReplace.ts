export function imgReplace (el: any, urls: string): void {
  const arr = urls.split('||')
  const currentIndex = arr.findIndex(item => {
    const url = new URL(item, document.baseURI)
    return url.href === el.src
  })
  if (currentIndex !== -1 && typeof arr[currentIndex + 1] !== 'undefined') {
    el.src = arr[currentIndex + 1]
  }
}

(window as any).imgReplace = imgReplace
