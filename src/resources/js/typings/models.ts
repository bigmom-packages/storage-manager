export interface BigmomFolder {
  name: string
  parentFqn: string
  fqn: string
}

export interface BigmomFile {
  name: string
  fqn: string
  parentFqn: string
  type: string
  size: number
  url: string
  urls: string[]
  activeVersionId: string
}

export interface BigmomFileVersion {
  versionId: string
  fqn: string
  parentFqn: string
  type: string
  size: number
  url: string
  urls: string[]
}
