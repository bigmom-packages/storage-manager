import { HTMLAttributes as BaseHTMLAttributes } from 'react'


declare module 'react' {
  interface HTMLAttributes<T> extends AriaAttributes, DOMAttributes<T>, BaseHTMLAttributes<T> {
    // extends React's HTMLAttributes
    directory?: string
    webkitdirectory?: string
    mozdirectory?: string
    odirectory?: string
    msdirectory?: string
  }
}

interface FileWithDirectory extends File {
  webkitRelativePath?: string
}
