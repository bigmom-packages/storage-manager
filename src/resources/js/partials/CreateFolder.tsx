import axios from 'axios'
import { useFormik } from 'formik'
import React, { ReactElement } from 'react'
import apiSetFieldError from '../lib/Formik/helpers'
import { baseSchema as schema } from '../lib/Formik/validationSchemas/folder'
import Input from '../components/atoms/Input'
import { PrimaryButton } from '../components/atoms/Button'
import { ErrorMessage } from '../components/atoms/ErrorMessage'
import { errorMessage } from '../lib/defaults'
import { route } from '../lib/helpers'

export default function CreateFolder ({ parentFqn, onSuccess }: { parentFqn: string | null, onSuccess?: () => void }): ReactElement {
  const form = useFormik({
    initialValues: {
      name: ''
    },
    onSubmit: async (values, { setFieldError }) => {
      try {
        await axios.post(route('/api/folder/create'), { ...values, parentFqn })

        if (typeof onSuccess !== 'undefined') {
          onSuccess()
        }
      } catch (err) {
        if (err.response.status === 422) {
          apiSetFieldError(err.response.data.errors, setFieldError)
        } else {
          console.error(err)
          setFieldError('name', errorMessage)
        }
      }
    },
    validationSchema: schema
  })

  return (
    <form onSubmit={form.handleSubmit}>
      <p className="text-lg font-bold mx-auto">Name your new folder:</p>
      <Input type="text" name="name" className="w-full mb-2" placeholder="Folder name" onChange={form.handleChange} onBlur={form.handleBlur} />
      <PrimaryButton className="w-full">Create</PrimaryButton>
      <ErrorMessage className="h-8">{form.errors.name}</ErrorMessage>
    </form>
  )
}
