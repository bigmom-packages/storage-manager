import React, { ReactElement, useEffect, useState } from 'react'
import cn from 'classnames'
import { FileWithDirectory } from '../typings/decs'
import { TH } from '../components/atoms/TH'
import { TD } from '../components/atoms/TD'
import { useDisks } from '../lib/SWR/hooks'
import Loading from '../components/molecules/Loading'
import Error from '../components/molecules/Error'
import { FileMap } from '../pages/Upload'
import _ from 'lodash'
import UploadItem from '../components/molecules/UploadItem'

interface UploadTableProps {
  fileMap: FileMap
  setUploadHasEnded: (status: boolean) => void
  visibilityIsPublic: boolean
  isDownloadable: boolean
}

export default function UploadTable ({ fileMap, setUploadHasEnded, visibilityIsPublic, isDownloadable }: UploadTableProps): ReactElement {
  const { disks, isLoading, error } = useDisks()
  const [uploadPointer, setUploadPointer] = useState(3)

  useEffect(() => {
    if (typeof disks !== 'undefined') {
      if (uploadPointer >= ((Object.keys(fileMap).length * Object.keys(disks).length + 3))) {
        setUploadHasEnded(true)
      }
    }
  }, [uploadPointer])

  function getPath (file: FileWithDirectory): string {
    const relativePath = file.webkitRelativePath
    return typeof relativePath !== 'undefined' && relativePath !== ''
      ? relativePath
      : file.name
  }

  function incrementPointer () {
    setUploadPointer(uploadPointer => uploadPointer + 1)
  }

  if (isLoading) return <Loading />
  if (typeof error !== 'undefined') return <Error message={error.message} />

  if (fileMap === {}) return <Loading />

  return (
    <table className="w-full table-collapse bg-white mb-8">
      <thead>
        <tr>
          <TH>Name</TH>
          {Object.keys(disks).map(disk => <TH key={`head-${disk}`}>{disk}</TH>)}
        </tr>
      </thead>
      <tbody>
        {Object.keys(fileMap).map((destination, fileIndex) => (
          <tr key={`row-${destination}`}>
            <TD>{getPath(fileMap[destination])}</TD>
            {Object.keys(disks).map((disk, diskIndex) => (
              <TD key={`row-${destination}-disk-${disk}`} className="border-l">
                <UploadItem
                  disk={disk}
                  destination={destination}
                  file={fileMap[destination]}
                  shouldUpload={((fileIndex * Object.keys(disks).length) + diskIndex) < uploadPointer}
                  handleUploadDone={incrementPointer}
                  visibilityIsPublic={visibilityIsPublic}
                  isDownloadable={isDownloadable}
                />
              </TD>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  )
}
