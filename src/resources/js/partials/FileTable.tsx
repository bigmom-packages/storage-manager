import React, { ReactElement, TdHTMLAttributes, ThHTMLAttributes, useEffect, useState } from 'react'
import cn from 'classnames'
import prettyBytes from 'pretty-bytes'
import { FileWithDirectory } from '../typings/decs'
import { TH } from '../components/atoms/TH'
import { TD as BaseTD } from '../components/atoms/TD'
import { SecondaryButton, TertiaryButton } from '../components/atoms/Button'

interface FileTableProps {
  fileMap: Record<string, FileWithDirectory>
  removeFile: (path: string) => void
}

export default function FileTable ({ fileMap, removeFile }: FileTableProps): ReactElement {
  const [fileArr, setFileArr] = useState<FileWithDirectory[]>([])

  useEffect(() => {
    const newFileArr: File[] = []
    Object.keys(fileMap).forEach(key => {
      newFileArr.push(fileMap[key])
    })
    setFileArr(newFileArr)
  }, [fileMap])

  function getPath (file: FileWithDirectory): string {
    const path = file.webkitRelativePath
    
    if (typeof path !== 'undefined' && path !== '') return path

    return file.name
  }

  return (
    <table className="w-full table-collapse bg-white mb-8">
      <thead>
        <tr>
          <TH>Name</TH>
          <TH>Type</TH>
          <TH>Size</TH>
          <TH></TH>
        </tr>
      </thead>
      <tbody>
        {fileArr.length > 0
          ? fileArr.map(file => (
            <tr key={`file-table-${getPath(file)}`}>
              <TD>{getPath(file)}</TD>
              <TD>{file.type}</TD>
              <TD>{prettyBytes(file.size)}</TD>
              <TD><TertiaryButton onClick={() => { removeFile(getPath(file)) }}>Remove</TertiaryButton></TD>
            </tr>
          ))
          : <tr><TD colSpan={4} className="text-center py-6">No items selected.</TD></tr>}
      </tbody>
    </table>
  )
}

export function TD (props: TdHTMLAttributes<HTMLTableCellElement>): ReactElement<HTMLTableCellElement> {
  return (
    <BaseTD {...props} className={cn(props.className, 'border-l')} />
  )
}
