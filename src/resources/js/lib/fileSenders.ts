import axios from 'axios'

/**
 * Modified version of laravel-vapor (https://www.npmjs.com/package/laravel-vapor)
 * @param file 
 * @param disk 
 * @param presignUrl 
 * @param options 
 * @returns any
 */
export async function presignAndSend (file: File, disk: string, presignUrl: string, options: Record<string, any> = {}) {
  const response = await axios.post(presignUrl, {
    'content_type': options.contentType || file.type,
    'expires': options.expires || '',
    'visibility': options.visibility || '',
    'disk': disk,
    ...options.data
  }, {
    headers: options.headers || {},
    ...options.options,
    withCredentials: false
  })

  let headers = response.data.headers

  if ('Host' in headers) delete headers.Host

  if (typeof options.progress === 'undefined') options.progress = () => {}

  const cancelToken = options.cancelToken || ''

  await axios.put(response.data.url, file, {
    cancelToken: cancelToken,
    headers: headers,
    onUploadProgress: (progressEvent) => {
      options.progress(progressEvent.loaded / progressEvent.total)
    }
  })

  response.data.extension = file.name.split('.').pop()

  return response.data
}
