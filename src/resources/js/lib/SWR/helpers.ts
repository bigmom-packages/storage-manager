import axios, { AxiosError, AxiosRequestConfig } from 'axios'

export async function apiFetcher (url: string, config: AxiosRequestConfig | undefined): Promise<any> {
  return await axios.get(url, { withCredentials: true, ...config })
    .then(res => res.data)
    .catch((err: AxiosError) => {
      const error = new Error('An error occured while fetching the data.') as APIError
      error.info = err.message
      error.status = err.code
      throw error
    })
}

export interface APIError extends Error {
  info: string
  status?: string
}

export default function getIsLoading (data: any, error: any): boolean {
  return typeof data === 'undefined' && typeof error === 'undefined'
}
