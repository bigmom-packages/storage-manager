import { route } from "../helpers"
import useSWR, { mutate, SWRResponse } from 'swr'
import getIsLoading, { APIError, apiFetcher } from "./helpers"

export function useFolder (fqn: string) {
  let path = route(`/api/folder`)
  const params = new URLSearchParams
  params.append('folder', fqn)
  path = path + '?' + params.toString()
  const { data, error }: SWRResponse<any, APIError> = useSWR(path, apiFetcher)
  const isLoading = getIsLoading(data, error)

  function updateFolder (): void {
    mutate(path)
  }

  return {
    folder: data,
    error: error,
    isLoading: isLoading,
    updateFolder: updateFolder
  }
}

export function useDisks () {
  let path = route(`/api/disks`)
  const { data, error }: SWRResponse<any, APIError> = useSWR(path, apiFetcher, {
    revalidateOnReconnect: false,
    revalidateOnFocus: false
  })
  const isLoading = getIsLoading(data, error)

  return {
    disks: data,
    error: error,
    isLoading: isLoading
  }
}

export function useFile (fqn: string) {
  let path = route('/api/file')
  const params = new URLSearchParams
  params.append('file', fqn)
  path = path + '?' + params.toString()
  const { data, error }: SWRResponse<any, APIError> = useSWR(path, apiFetcher)
  const isLoading = getIsLoading(data, error)

  function updateFile (): void {
    mutate(path)
  }

  return {
    file: data,
    error: error,
    isLoading: isLoading,
    updateFile: updateFile
  }
}
