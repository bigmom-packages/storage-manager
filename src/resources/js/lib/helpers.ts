export function route (path: string): string {
  if (path.charAt(0) === '/') {
    path = path.substr(1)
  }
  return '/bigmom/storage-manager/' + path
}
