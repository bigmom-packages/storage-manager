import * as yup from 'yup'

export const baseSchema = yup.object().shape({
  name: yup.string().required().max(255)
})
