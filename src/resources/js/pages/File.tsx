import { DocumentDuplicateIcon } from '@heroicons/react/outline'
import axios from 'axios'
import { ReactElement, ReactNode, useEffect, useMemo, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { DangerButton, TertiaryButton } from '../components/atoms/Button'
import Card from '../components/atoms/Card'
import FileVersionItem from '../components/atoms/FileVersionItem'
import BreadCrumb from '../components/molecules/BreadCrumb'
import Error from '../components/molecules/Error'
import Loading from '../components/molecules/Loading'
import { route } from '../lib/helpers'
import { useFile } from '../lib/SWR/hooks'
import { BigmomFileVersion } from '../typings/models'

export default function File (): ReactElement {
  const { search: searchParams } = useLocation()
  const fileFqn = useMemo(() => {
    const params = new URLSearchParams(searchParams)
    const fqn = params.get('file')
    if (fqn === null) return ''
    return fqn
  }, [searchParams])
  const { file, isLoading, error, updateFile } = useFile(fileFqn)
  const [deleteStatus, setDeleteStatus] = useState(0)
  const history = useHistory()

  if (isLoading) return <Loading />
  if (typeof error !== 'undefined') return <Error message={error.message} />

  function isActive (version: BigmomFileVersion) {
    return file.activeVersionId === version.versionId
  }

  function setAsActive (version: BigmomFileVersion) {
    axios.post(route('api/file/active'), { versionId: version.versionId, file: file.fqn })
      .then(() => { updateFile() })
      .catch(console.error)
  }

  function getDeleteSection (): ReactNode {
    switch (deleteStatus) {
      case 0: return <DangerButton onClick={() => { setDeleteStatus(1) }} className="mr-4">Delete</DangerButton>
      case 1: return (<div className="flex mr-4">
          <DangerButton onClick={() => { setDeleteStatus(2); deleteFile() }}>Confirm Delete</DangerButton>
          <TertiaryButton onClick={() => { setDeleteStatus(0) }} className="ml-4">Cancel</TertiaryButton>
        </div>)
      case 2: return (<p>Deleting...</p>)
    }
  }

  function deleteFile () {
    const folderFqn = file.parentFqn
    axios.post(route('api/file/delete'), { file: file.fqn })
      .then(() => { history.push(route(`?folder=${folderFqn}`)) })
      .catch(console.error)
  }

  return (
    <>
      <div className="container mx-auto py-6">
        <BreadCrumb fqn={file.fqn} />
        <Card className="w-full mb-8">
          <div className="flex justify-between items-center">
            <div>
              <h4 className="text-lg"><strong className="font-bold">{file.name}</strong></h4>
              <p className="my-4">FQN: {file.fqn}</p>
            </div>
            {getDeleteSection()}
          </div>
          <div className="md:flex">
            <div className="py-4 mr-8">
              <p className="font-bold">Type</p>
              <p>{file.type}</p>
            </div>
            <div className="py-4 mr-12">
              <p className="font-bold">Size</p>
              <p>{file.size}</p>
            </div>
            <div className="py-4">
              <p className="font-bold">URLs</p>
              <ol className="list-decimal px-2">
                {file.urls.map((url: string) => <li key={`file-url-${url}`}>
                  <a href={url} className="text-blue-600 hover:text-blue-400">{url}</a>
                  <DocumentDuplicateIcon className="hover:text-gray-700 h-6 w-6" />
                </li>)}
              </ol>
            </div>
          </div>
        </Card>
        {file.versions.length > 0
          ? <>
            <div className="w-full px-4 py-4">
              <h4 className="text mr-6">Versions</h4>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-4 gap-4 w-full">
              {file.versions.map((version: BigmomFileVersion) => (
                <FileVersionItem key={`file-${version.fqn}`} text={version.versionId} version={version} isActive={isActive(version)} setAsActive={setAsActive} />
              ))}
            </div>
          </>
          : <p className="text-gray-400 w-full text-center">Empty</p>}
      </div>
    </>
  )
}
