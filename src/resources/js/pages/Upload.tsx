import React, { ReactElement, useEffect, useMemo, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { PrimaryButton, SecondaryButton } from '../components/atoms/Button'
import Card from '../components/atoms/Card'
import BreadCrumb from '../components/molecules/BreadCrumb'
import Error from '../components/molecules/Error'
import Loading from '../components/molecules/Loading'
import { useDisks, useFolder } from '../lib/SWR/hooks'
import _ from 'lodash'
import FileTable from '../partials/FileTable'
import { FileWithDirectory } from '../typings/decs'
import axios from 'axios'
import { route } from '../lib/helpers'
import UploadTable from '../partials/UploadTable'

export type FileMap = Record<string, FileWithDirectory>

export default function Upload (): ReactElement {
  const { search: searchParams, pathname } = useLocation()
  const [folderId, setFolderId] = useState('/')
  const [fileMap, setFileMap] = useState<FileMap>({})
  const [uploadHasStarted, setUploadHasStarted] = useState(false)
  const [fileVersionMap, setFileVersionMap] = useState<FileMap>({})
  const [uploadHasEnded, setUploadHasEnded] = useState(false)
  const [visibilityIsPublic, setVisibilityIsPublic] = useState(false)
  const [isDownloadable, setIsDownloadable] = useState(false)
  
  useEffect(() => {
    const params = new URLSearchParams(searchParams)
    const folderId = params.get('folder')
    if (folderId !== null && folderId.trim() !== '') {
      setFolderId(folderId)
    } else {
      const params = new URLSearchParams
      params.append('folder', '/')
      const url = new URL(location.origin + pathname + '?' + params.toString())
      history.pushState({}, '', url.toString())
    }
  }, [searchParams])

  const { folder, error, isLoading } = useFolder(folderId)

  function addFiles (fileList: FileList): void {
    const newFileMap = _.cloneDeep(fileMap)
    Array.from(fileList).map((file: FileWithDirectory) => {
      const fileMapKey = getPath(file)
      newFileMap[fileMapKey] = file
    })
    setFileMap(newFileMap)
  }

  function removeFile (path: string): void {
    const newFileMap = _.cloneDeep(fileMap)
    delete newFileMap[path]
    setFileMap(newFileMap)
  }

  function getPath (file: FileWithDirectory): string {
    const relativePath = file.webkitRelativePath
    return typeof relativePath !== 'undefined' && relativePath !== ''
      ? relativePath
      : file.name
  }

  function handleSubmit () {
    try {
      Object.keys(fileMap).forEach(async key => {
        const file = (fileMap[key])
        const res = await axios.put(route('api/file/put'), {
          fqn: folder.fqn + getPath(file),
          type: file.type,
          size: file.size,
          visibility: visibilityIsPublic ? 'public-read' : 'private',
          is_downloadable: isDownloadable ? 1 : 0
        })
        const fqn = res.data.fqn
        setFileVersionMap(map => { return { ...map, [fqn]: file } })
      })
      setUploadHasStarted(true)
    } catch (err) {
      console.error(err)
    }
  }

  if (isLoading) return <Loading />
  if (typeof error !== 'undefined') return <Error message={error.message} />

  return (
    <>
      <div className="container mx-auto py-6">
        <BreadCrumb fqn={folder.fqn} lastCrumbIsLink />
        <Card className="w-full mb-8 flex justify-between items-center">
          <h4 className="text-lg">Upload files to <strong className="font-bold">{folder.name !== '/' ? folder.name : 'Root folder'}</strong></h4>
          <div className="flex">
            {uploadHasStarted
              ? (uploadHasEnded
                ? <Link to={route(`/?folder=${folder.fqn}`)}><SecondaryButton>Close</SecondaryButton></Link>
                : '')
              : (<>
                <label htmlFor="folder-upload-btn"><SecondaryButton Component="div" className="cursor-pointer">Upload Folder</SecondaryButton></label>
                <input
                  type="file"
                  id="folder-upload-btn"
                  className="opacity-0 h-0 w-0"
                  multiple
                  webkitdirectory=""
                  directory=""
                  mozdirectory=""
                  odirectory=""
                  msdirectory=""
                  onChange={(evt) => {
                    const fileList = evt.currentTarget.files
                    if (fileList !== null) {
                      addFiles(fileList)
                    }
                  }}
                />
                <label htmlFor="files-upload-btn"><PrimaryButton Component="div" className="cursor-pointer">Upload Files</PrimaryButton></label>
                <input
                  type="file"
                  id="files-upload-btn"
                  className="opacity-0 h-0 w-0"
                  multiple
                  onChange={(evt) => {
                    const fileList = evt.currentTarget.files
                    if (fileList !== null) {
                      addFiles(fileList)
                    }
                  }}
                />
              </>)}
          </div>
        </Card>
        {uploadHasStarted
          ? (<>
              <UploadTable
                fileMap={fileVersionMap}
                setUploadHasEnded={setUploadHasEnded}
                visibilityIsPublic={visibilityIsPublic}
                isDownloadable={isDownloadable}  
              />
            </>)
          : (<>
              <Card className="w-full mb-8 flex">
                <div className="flex-1">
                  <label htmlFor="visibility" className="mr-4">Is publicly accessible?</label>
                  <input id="visibility" type="checkbox" checked={visibilityIsPublic} onChange={(evt) => {
                    setVisibilityIsPublic(evt.currentTarget.checked)
                  }} />
                </div>
                <div className="flex-1">
                  <label htmlFor="is_downloadable" className="mr-4">Is downloadable?</label>
                  <input id="is_downloadable" type="checkbox" checked={isDownloadable} onChange={(evt) => {
                    setIsDownloadable(evt.currentTarget.checked)
                  }} />
                </div>
              </Card>
              <FileTable fileMap={fileMap} removeFile={removeFile} />
              {Object.keys(fileMap).length > 0 ? <PrimaryButton onClick={handleSubmit}>Submit</PrimaryButton> : <></>}
            </>)}
      </div>
    </>
  )
}
