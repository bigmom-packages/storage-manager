import axios from 'axios'
import React, { ReactElement, ReactNode, useEffect, useRef, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import { DangerButton, PrimaryButton, SecondaryButton, TertiaryButton } from '../components/atoms/Button'
import Card from '../components/atoms/Card'
import FileItem from '../components/atoms/FileItem'
import FolderItem from '../components/atoms/FolderItem'
import BreadCrumb from '../components/molecules/BreadCrumb'
import Error from '../components/molecules/Error'
import Loading from '../components/molecules/Loading'
import { route } from '../lib/helpers'
import { useFolder } from '../lib/SWR/hooks'
import CreateFolder from '../partials/CreateFolder'
import { BigmomFolder, BigmomFile } from '../typings/models'

export default function Folder (): ReactElement {
  const [folderId, setFolderId] = useState<string>('/')
  const [createFolderIsShown, setCreateFolderIsShown] = useState(false)
  const { folder, error, isLoading, updateFolder } = useFolder(folderId)
  const [childFolders, setChildFolders] = useState<BigmomFolder[]>([])
  const [files, setFiles] = useState<BigmomFile[]>([])
  const [deleteStatus, setDeleteStatus] = useState(0)
  const { search: searchParams, pathname } = useLocation()
  const history = useHistory()
  useEffect(() => {
    const params = new URLSearchParams(searchParams)
    const folderId = params.get('folder')
    if (folderId !== null && folderId.trim() !== '') {
      setFolderId(folderId)
    } else {
      history.push(route(`?folder=/`))
    }
  }, [searchParams])
  useEffect(() => {
    if (typeof folder !== 'undefined') {
      const newChildFolders: BigmomFolder[] = []
      folder.childFolders.forEach((child: BigmomFolder) => {
        newChildFolders.push(child)
      })
      setChildFolders(newChildFolders)

      const newFiles: BigmomFile[] = []
      folder.files.forEach((file: BigmomFile) => {
        newFiles.push(file)
      })
      setFiles(newFiles)
    }
  }, [folder])

  if (isLoading) return <Loading />
  if (typeof error !== 'undefined') return <Error message={error.message} />

  function getUploadLink (): string {
    const path = route(`/upload`)
    const params = new URLSearchParams
    params.append('folder', folderId)
    return path + '?' + params.toString()
  }

  function getDeleteSection (): ReactNode {
    switch (deleteStatus) {
      case 0: return <DangerButton onClick={() => { setDeleteStatus(1) }} className="mr-4">Delete</DangerButton>
      case 1: return (<div className="flex mr-4">
          <DangerButton onClick={() => { setDeleteStatus(2); deleteFolder() }}>Confirm Delete</DangerButton>
          <TertiaryButton onClick={() => { setDeleteStatus(0) }} className="ml-4">Cancel</TertiaryButton>
        </div>)
      case 2: return (<p className="mr-4">Deleting...</p>)
    }
  }

  function deleteFolder (): void {
    const parentFqn = folder.parentFqn
    axios.post(route('api/folder/delete'), { folder: folder.fqn })
      .then(() => { history.push(route(`?folder=${parentFqn}`)) })
      .catch(console.error)
  }

  return (
    <>
      <div className="container mx-auto py-6">
        <BreadCrumb fqn={folderId} />
        <Card className="w-full mb-4 flex justify-between items-center">
          <h4 className="text-lg mr-8">{folder.name !== '/' ? folder.name : 'Root folder'}</h4>
          <div className="flex items-center">
            {folder.fqn !== '/'
              ? getDeleteSection()
              : ''}
            <SecondaryButton onClick={() => { setCreateFolderIsShown(true) }}>New Folder</SecondaryButton>
            <Link to={getUploadLink()}><PrimaryButton>Upload</PrimaryButton></Link>
          </div>
        </Card>
        {childFolders.length > 0
          ? <>
            <div className="w-full px-4 py-4">
              <h4 className="text mr-6">Folders</h4>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-4 gap-4 w-full pb-4">
              {childFolders.map((folder: BigmomFolder) => (
                <Link key={`folder-${folder.fqn}`} to={route(`?folder=${folder.fqn}`)}>
                  <FolderItem text={folder.name} />
                </Link>
              ))}
            </div>
          </>
          : <></>}
        {files.length > 0
          ? <>
            <div className="w-full px-4 py-4">
              <h4 className="text mr-6">Files</h4>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-4 gap-4 w-full">
              {files.map((file: BigmomFile) => (
                <FileItem key={`file-${file.fqn}`} text={file.name} file={file} />
              ))}
            </div>
          </>
          : <></>}
        {childFolders.length === 0 && files.length === 0
          ? <p className="text-gray-400 w-full text-center">Empty</p>
          : <></>}
      </div>
      {createFolderIsShown
        ? <div className="absolute top-0 left-0 w-full h-full flex justify-center items-center">
            <div className="bg-black opacity-50 w-full h-full absolute top-0 left-0" onClick={() => { setCreateFolderIsShown(false) }} />
            <Card className="px-16 py-16 z-10">
              <CreateFolder parentFqn={folderId} onSuccess={() => { setCreateFolderIsShown(false); updateFolder() }} />
            </Card>
          </div>
        : <></>}
    </>
  )
}
