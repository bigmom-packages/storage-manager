<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Storage Manager - {{ config('app.name') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Style -->
    <link href="{{ asset('vendor/bigmom/storage-manager/css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('vendor/bigmom/storage-manager/js/index.js') }}" defer></script>
  </head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <input id="back-url" type="hidden" data-url="{{ config('storage-manager.back-url') }}">
    <div id="app" class="h-full bg-gray-100 overflow-auto"></div>
  </body>
</html>
