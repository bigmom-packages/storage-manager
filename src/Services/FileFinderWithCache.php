<?php

namespace Bigmom\StorageManager\Services;

use Bigmom\StorageManager\Exceptions\FileNotFoundException;
use Bigmom\StorageManager\Models\BigmomFile;
use Bigmom\StorageManager\Models\BigmomFileVersion;
use Illuminate\Support\Facades\Cache;

class FileFinderWithCache
{
    public static function find(string $fqn)
    {
        if (config('app.env') === 'local') {
            $result = SELF::getFromDB($fqn);
        } else {
            $result = Cache::remember("storage-manager.$fqn", 60, function () use ($fqn) {
                return SELF::getFromDB($fqn);
            });
        }

        if ($result) return $result;

        throw new FileNotFoundException($fqn, 1);
    }

    protected static function getFromDB(string $fqn)
    {
        $file = BigmomFile::where('fqn', $fqn)->first()->load('activeVersion');
        if ($file) return $file;
        $version = BigmomFileVersion::where('fqn', $fqn)->first();
        if ($version) return $version;
    }
}
