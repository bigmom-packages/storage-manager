<?php

namespace Bigmom\StorageManager\Services;

use Bigmom\StorageManager\Contracts\Deconstructor;

class FqnDeconstructor implements Deconstructor
{
    public function deconstruct(string $item): array
    {
        $oldArray = explode('/', $item);
        
        $string = $oldArray[0];

        $newArray = [];

        if (count($oldArray) > 1) {
            for ($i = 1; $i < count($oldArray); $i++) {
                array_push($newArray, $string . '/');
                $string .= "/{$oldArray[$i]}";
            }
        }

        array_push($newArray, $string);

        return array_values(array_unique($newArray));
    }
}
