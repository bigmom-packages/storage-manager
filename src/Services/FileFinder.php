<?php

namespace Bigmom\StorageManager\Services;

use Bigmom\StorageManager\Exceptions\FileNotFoundException;
use Bigmom\StorageManager\Models\BigmomFile;
use Bigmom\StorageManager\Models\BigmomFileVersion;

class FileFinder
{
    public static function find(string $fqn)
    {
        $file = BigmomFile::where('fqn', $fqn)->first();
        if ($file) return $file;
        $version = BigmomFileVersion::where('fqn', $fqn)->first();
        if ($version) return $version;

        throw new FileNotFoundException($fqn, 1);
    }
}
