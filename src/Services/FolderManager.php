<?php

namespace Bigmom\StorageManager\Services;

use Bigmom\StorageManager\Actions\DeleteFiles;
use Bigmom\StorageManager\Contracts\Deconstructor;
use Bigmom\StorageManager\Models\BigmomFolder;

class FolderManager
{
    public function getAndCreateIfNotExist(string $fqn, Deconstructor $deconstructor): BigmomFolder
    {
        $deconstructedFqn = $deconstructor->deconstruct($fqn);

        $folder = BigmomFolder::where('fqn', $fqn)->first();

        if ($folder) return $folder;

        $parentFqn = $deconstructedFqn[count($deconstructedFqn) - 2];

        $parentFolder = $this->getAndCreateIfNotExist($parentFqn, $deconstructor);

        $splitFqn = explode('/', $fqn);

        $name = $splitFqn[count($splitFqn) - 2];

        return BigmomFolder::create([
            'name' => $name,
            'parent_id' => $parentFolder->id,
            'parent_fqn' => $parentFqn,
            'fqn' => $fqn,
        ]);
    }

    public function deleteFolder(BigmomFolder $folder)
    {
        foreach ($folder->files as $file) {
            foreach ($file->versions as $version) {
                DeleteFiles::delete($version);
                $version->delete();
            }
            $file->delete();
        }

        foreach ($folder->children as $child) {
            $this->deleteFolder($child);
        }

        $folder->delete();
    }
}
