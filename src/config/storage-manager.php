<?php

return [

    'expose-console' => env('STORAGE_MANAGER_HAS_CONSOLE', false),

    // URL for back button in console
    'back-url' => '/bigmom',

    'disks' => [
        
        'sequence' => [
            'local',
            // 's3',
        ],

        /**
         * Disk configs:
         * 
         * is_presigned. If true, need presign_url
         * presign_url. Url to send get presigned url request to
         * folder. Folder to use
         */
        'config' => [

            'local' => [
                'is_presigned' => false
            ],

            // 's3' => [
            //     'is_presigned' => true,
            //     'presign_url' => '/bigmom/storage-manager/api/file/presign-url',
            //     'folder' => '/assets',
            // ],

        ],

    ],

    'files' => [

        'max-size' => 5000000000,
        
    ],

    // Exposes an endpoint to retrieve all folders, files, and file versions.
    'allow-pull' => true,

    // Put in .env if trying to pull database tables from another server and if you are planning on using command provided by this package.
    // Warning: This package's pulling function uses Basic Auth.
    'api' => [
        'url' => env('STORAGE_MANAGER_API_URL'),
        'username' => env('STORAGE_MANAGER_API_USERNAME'),
        'password' => env('STORAGE_MANAGER_API_PASSWORD'),
    ],

    'middleware' => [

        'console' => [
            'web',
        ],

        'api-pull' => [
            'api',
        ],

    ],

    'use-cache-facade' => false,

];
