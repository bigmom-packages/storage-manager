<?php

namespace Bigmom\StorageManager\Providers;

use Bigmom\StorageManager\Commands\PullStorageManager;
use Bigmom\StorageManager\View\Components\Image;
use Bigmom\StorageManager\Services\FileFinder;
use Bigmom\StorageManager\Services\FileFinderWithCache;
use Illuminate\Support\ServiceProvider;
use Bigmom\StorageManager\Contracts\Deconstructor;
use Bigmom\StorageManager\Facades\FileFinder as FacadesFileFinder;
use Bigmom\StorageManager\Services\FqnDeconstructor;
use Bigmom\StorageManager\View\Components\Source;
use Illuminate\Support\Facades\Blade;

class StorageManagerServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Deconstructor::class, function ($app) {
            return new FqnDeconstructor;
        });

        $this->app->singleton('file-finder', function ($app) {
            return config('storage-manager.use-cache-facade', false) ? new FileFinderWithCache : new FileFinder;
        });
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__.'/../migrations');

            $this->publishes([
                __DIR__.'/../config/storage-manager.php' => config_path('storage-manager.php'),
            ], 'config');

            $this->publishes([
                __DIR__.'/../public' => public_path('vendor/bigmom/storage-manager'),
            ], 'public');

            $this->commands([
                PullStorageManager::class,
            ]);
        }

        $this->loadRoutesFrom(__DIR__.'/../routes.php');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'bigmom-storage-manager');

        Blade::directive('url', function ($fqn) {
            return e(FacadesFileFinder::find(substr($fqn, 1, strlen($fqn) - 2))->url);
        });

        $this->loadViewComponentsAs('bigmom', [
            Image::class,
            Source::class,
        ]);
    }
}
