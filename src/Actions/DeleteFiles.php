<?php

namespace Bigmom\StorageManager\Actions;

use Bigmom\StorageManager\Models\BigmomFileVersion;
use Illuminate\Support\Facades\Storage;

class DeleteFiles
{
    public static function delete(BigmomFileVersion $version)
    {
        $disks = config('storage-manager.disks.sequence');
        $fqn = $version->fqn . '.' . $version->extension;

        foreach ($disks as $disk) {
            Storage::disk($disk)->delete($fqn);
        }

        return true;
    }
}
