<?php

namespace Bigmom\StorageManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BigmomFile extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'bigmom_folder_id',
        'parent_fqn',
        'fqn',
        'active_version_id',
        'created_at',
        'updated_at',
    ];

    public function parent()
    {
        return $this->belongsTo(BigmomFolder::class);
    }

    public function versions()
    {
        return $this->hasMany(BigmomFileVersion::class);
    }

    public function activeVersion()
    {
        return $this->hasOne(BigmomFileVersion::class, 'version_id', 'active_version_id');
    }

    public function getSizeAttribute()
    {
        return $this->activeVersion->size;
    }

    public function getTypeAttribute()
    {
        return $this->activeVersion->type;
    }

    public function getUrlAttribute()
    {
        return $this->activeVersion->url;
    }

    public function getUrlsAttribute()
    {
        return $this->activeVersion->urls;
    }
}
