<?php

namespace Bigmom\StorageManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class BigmomFileVersion extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'version_id',
        'bigmom_file_id',
        'parent_fqn',
        'fqn',
        'type',
        'extension',
        'size',
        'is_public_read',
        'is_downloadable',
        'created_at',
        'updated_at',
    ];

    public function file()
    {
        return $this->belongsTo(BigmomFile::class);
    }

    public function getUrlAttribute()
    {
        return $this->urls[0];
    }

    public function getUrlsAttribute()
    {
        return collect(config('storage-manager.disks.sequence'))->map(function ($disk) {
            return $this->getUrl($disk);
        });
    }
    
    public function getUrl($disk)
    {
        $folderPrefix = config("storage-manager.disks.config.{$disk}.folder", '');
        $path = $folderPrefix . $this->fqn . '.' . $this->extension;
        if (substr($path, 0, 1) === '/') {
            $path = substr($path, 1);
        }
        return Storage::disk($disk)->url($path);
    }
}
