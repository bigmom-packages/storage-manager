<?php

namespace Bigmom\StorageManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BigmomFolder extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'parent_id',
        'parent_fqn',
        'fqn',
        'created_at',
        'updated_at',
    ];

    public function parent()
    {
        return $this->belongsTo(BigmomFolder::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(BigmomFolder::class, 'parent_id');
    }

    public function files()
    {
        return $this->hasMany(BigmomFile::class);
    }
}
