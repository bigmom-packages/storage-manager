<?php

namespace Bigmom\StorageManager\View\Components;

use Bigmom\StorageManager\Facades\FileFinder;
use Illuminate\View\Component;

class Source extends Component
{
    public $urls;
    public $type;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $fqn)
    {
        $file = FileFinder::find($fqn);
        $this->urls = $file->urls;
        $this->type = $file->type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('bigmom-storage-manager::components.source', [
            'urls' => $this->urls,
            'type' => $this->type,
        ]);
    }
}
